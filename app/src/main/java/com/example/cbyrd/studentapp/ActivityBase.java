/**
 *
 */

package com.example.cbyrd.studentapp;

import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;

abstract class ActivityBase extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    //
}
